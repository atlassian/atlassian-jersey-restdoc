package com.atlassian.jersey.wadl.doclet.options;

import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ClassDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.MethodDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ParamDocType;
import com.sun.jersey.wadl.resourcedoc.AggregatingDocProcessor;
import com.sun.jersey.wadl.resourcedoc.DocProcessor;
import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.Module;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.net.URL;
import java.util.List;

import static com.atlassian.jersey.wadl.doclet.options.Option.CLASSPATH;
import static com.atlassian.jersey.wadl.doclet.options.Option.DOC_PROCESSORS;
import static com.atlassian.jersey.wadl.doclet.options.Option.EXPAND;
import static com.atlassian.jersey.wadl.doclet.options.Option.MODULES;
import static com.atlassian.jersey.wadl.doclet.options.Option.OUTPUT;
import static java.util.stream.Collectors.toList;
import static org.codehaus.jackson.Version.unknownVersion;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertArrayEquals;

public class OptionsParserTest {

    private OptionsParser optionsParser;

    @Before
    public void setUp() {
        optionsParser = new OptionsParser();
    }

    @Test
    public void parse_whenAllOptionsGiven_shouldPopulateThemAll() throws Exception {
        // Arrange
        final String output = "theOutput";
        final File path1 = new File( "path1");
        final File path2 = new File( "path2");
        final String[][] input = {
                { OUTPUT.getKey(), output},
                { CLASSPATH.getKey(), "path1:path2" },
                { MODULES.getKey(), Module1.class.getName() + ":" + Module2.class.getName() },
                { EXPAND.getKey(), "expand1&expand2&expand3" },
                { DOC_PROCESSORS.getKey(), Processor1.class.getName() + ":" + Processor2.class.getName() }
        };

        // Act
        final Options options = optionsParser.parse(input);

        // Assert
        assertThat(options.getOutput(), is(output));
        final URL[] expectedURLs = { path1.toURI().toURL(), path2.toURI().toURL() };
        assertArrayEquals(expectedURLs, options.getClasspath());
        final List<Class<?>> moduleClasses = options.getCustomJacksonSerializerModules().stream()
                .map(Object::getClass)
                .collect(toList());
        assertThat(moduleClasses, hasItems(Module1.class, Module2.class));
        assertThat(options.getExpand(), hasItems("expand1", "expand2", "expand3"));
        final List<Class<?>> docProcessorClasses =
                ((AggregatingDocProcessor) options.getDocProcessor()).getDocProcessors().stream()
                        .map(Object::getClass)
                        .collect(toList());
        assertThat(docProcessorClasses, hasItems(Processor1.class, Processor2.class));
    }

    private static abstract class StubModule extends Module {

        @Override
        public String getModuleName() {
            return "theModuleName";
        }

        @Override
        public Version version() {
            return unknownVersion();
        }

        @Override
        public void setupModule(final SetupContext context) {
            // No-op
        }
    }

    static class Module1 extends StubModule {}

    static class Module2 extends StubModule {}

    private static class StubProcessor implements DocProcessor {

        @Override
        public Class<?>[] getRequiredJaxbContextClasses() {
            return new Class[0];
        }

        @Override
        public String[] getCDataElements() {
            return new String[0];
        }

        @Override
        public void processClassDoc(ClassDoc classDoc, ClassDocType classDocType) {

        }

        @Override
        public void processMethodDoc(MethodDoc methodDoc, MethodDocType methodDocType) {

        }

        @Override
        public void processParamTag(ParamTag paramTag, Parameter parameter, ParamDocType paramDocType) {

        }
    }

    static class Processor1 extends StubProcessor {}

    static class Processor2 extends StubProcessor {}
}