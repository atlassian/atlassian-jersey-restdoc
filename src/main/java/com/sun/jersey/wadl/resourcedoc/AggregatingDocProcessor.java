package com.sun.jersey.wadl.resourcedoc;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.unmodifiableList;

/**
 * A {@link DocProcessor} that delegates to multiple other {@link DocProcessor}s.
 *
 * Has to be in the {@code com.sun.jersey.wadl.resourcedoc} package in order to invoke
 * {@link DocProcessorWrapper#add(DocProcessor)}, which annoyingly is package-private.
 *
 * @since 2.0
 */
public class AggregatingDocProcessor extends DocProcessorWrapper {

    private final List<DocProcessor> docProcessors = new ArrayList<>();

    public AggregatingDocProcessor(final List<DocProcessor> docProcessors) {
        docProcessors.forEach(this::add);
    }

    @Override
    public void add(final DocProcessor docProcessor) {
        super.add(docProcessor);
        docProcessors.add(docProcessor);
    }

    /**
     * Returns the document processors that have been added to this aggregator.
     *
     * @return see description
     */
    @Nonnull
    public List<DocProcessor> getDocProcessors() {
        return unmodifiableList(docProcessors);
    }
}
