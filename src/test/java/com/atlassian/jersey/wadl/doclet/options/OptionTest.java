package com.atlassian.jersey.wadl.doclet.options;

import org.junit.Test;

import java.util.Optional;

import static com.atlassian.jersey.wadl.doclet.options.Option.MODULES;
import static com.atlassian.jersey.wadl.doclet.options.Option.OUTPUT;
import static com.atlassian.jersey.wadl.doclet.options.Option.length;
import static com.atlassian.jersey.wadl.doclet.options.Option.values;
import static java.util.Arrays.stream;
import static org.junit.Assert.assertEquals;

public class OptionTest {

    @Test
    public void length_whenGivenKnownOptions_shouldReturnTwo() {
        stream(Option.values()).forEach(option ->
                assertEquals(2, length(option.getKey())));
    }

    @Test
    // Requirement of the doclet contract
    public void length_whenGivenUnknownOption_shouldReturnZero() {
        assertEquals(0, length("fubar"));
    }

    @Test
    public void getValue_whenOptionNotPresent_shouldReturnEmpty() {
        // Arrange
        final Option option = values()[0];
        final String[][] options = {};

        // Act
        final Optional<String> value = option.getValue(options);
        assertEquals(Optional.empty(), value);
    }

    @Test
    public void getValue_whenOptionArrayTooShort_shouldReturnEmpty() {
        // Arrange
        final Option option = values()[0];
        final String[][] options = { {option.getKey() } };

        // Act
        final Optional<String> value = option.getValue(options);
        assertEquals(Optional.empty(), value);
    }

    @Test
    public void getValue_whenOptionArrayTooLong_shouldReturnEmpty() {
        // Arrange
        final Option option = values()[0];
        final String[][] options = { {option.getKey(), "theValue", "extra" } };

        // Act
        final Optional<String> value = option.getValue(options);
        assertEquals(Optional.empty(), value);
    }

    @Test
    public void getValue_whenValueIsNull_shouldReturnEmpty() {
        // Arrange
        final Option option = values()[0];
        final String[][] options = { {option.getKey(), null } };

        // Act
        final Optional<String> value = option.getValue(options);
        assertEquals(Optional.empty(), value);
    }

    @Test
    public void getValue_whenOptionArrayCorrectLength_shouldReturnValue() {
        // Arrange
        final Option option = values()[0];
        final String value = "theValue";
        final String[][] options = {
                {"baz", "bat"},
                {option.getKey(), value },
                {"foo", "bar"}
        };

        // Act
        final Optional<String> maybeValue = option.getValue(options);
        assertEquals(Optional.of(value), maybeValue);
    }

    private static void assertInvalid(
            final Option option, final String expectedError, final String[]... options) {
        assertEquals(Optional.of(expectedError), option.validate(options));
    }

    private static void assertValid(final Option option, final String[]... options) {
        assertEquals(Optional.empty(), option.validate(options));
    }

    @Test
    public void validate_whenMandatoryOptionIsMissing_shouldFail() {
        final String[][] options = {};
        assertInvalid(OUTPUT, "-output <path-to-file> must be specified.", options);
    }

    @Test
    public void validate_whenMandatoryOptionIsNull_shouldFail() {
        final String[][] options = { {OUTPUT.getKey(), null } };
        assertInvalid(OUTPUT, "-output <path-to-file> must be specified.", options);
    }

    @Test
    public void validate_whenMandatoryOptionIsBlank_shouldFail() {
        final String[][] options = { {OUTPUT.getKey(), " \t\n\r" } };
        assertInvalid(OUTPUT, "-output <path-to-file> must be specified.", options);
    }

    @Test
    public void validate_whenMandatoryOptionIsValid_shouldPass() {
        final String[][] options = { {OUTPUT.getKey(), "theOutput" } };
        assertValid(OUTPUT, options);
    }

    @Test
    public void validate_whenNonMandatoryOptionIsMissing_shouldPass() {
        final String[][] options = {};
        assertValid(MODULES, options);
    }

    @Test
    public void validate_whenNonMandatoryOptionIsBlank_shouldFail() {
        final String[][] options = { { MODULES.getKey(), " \t\r\n " } };
        assertInvalid(MODULES, "-modules if provided cannot be blank.", options);
    }

    @Test
    public void validate_whenNonMandatoryOptionIsValid_shouldPass() {
        final String[][] options = { { MODULES.getKey(), "module1:module2" } };
        assertValid(MODULES, options);
    }
}
