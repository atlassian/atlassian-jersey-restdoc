package com.atlassian.jersey.wadl.doclet.internal;

import com.atlassian.jersey.wadl.doclet.ResourceDocletJSON;
import com.atlassian.jersey.wadl.doclet.options.Options;
import com.atlassian.plugins.rest.common.expand.EntityCrawler;
import com.atlassian.plugins.rest.common.expand.SelfExpandingExpander;
import com.atlassian.plugins.rest.common.expand.parameter.DefaultExpandParameter;
import com.atlassian.plugins.rest.common.expand.resolver.ChainingEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.CollectionEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.EntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.ExpandConstraintEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.IdentityEntityExpanderResolver;
import com.atlassian.plugins.rest.common.expand.resolver.ListWrapperEntityExpanderResolver;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JaxbJsonMarshaller;
import com.sun.javadoc.AnnotationDesc;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.MemberDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.ParamTag;
import com.sun.javadoc.Parameter;
import com.sun.javadoc.ProgramElementDoc;
import com.sun.javadoc.SeeTag;
import com.sun.javadoc.Tag;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.AnnotationDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ClassDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.MethodDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.NamedValueType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ParamDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.RepresentationDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.RequestDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResponseDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.WadlParamType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.xml.namespace.QName;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.currentThread;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.function.Function.identity;
import static java.util.regex.Pattern.compile;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

/**
 * Generates REST documentation based on inputs from the user and the Javadoc framework.
 *
 * This is the main logic of this project.
 *
 * @since 2.0
 */
public class ResourceDocGenerator {

    static final String TAG_RESPONSE_PARAM = "response.param";

    private static final Logger LOG = LoggerFactory.getLogger(ResourceDocGenerator.class);

    private static final Pattern PATTERN_RESPONSE_REPRESENTATION = compile("@response\\.representation\\.([\\d]+)\\..*");

    private final Options options;

    public ResourceDocGenerator(final Options options) {
        this.options = requireNonNull(options);
    }

    /**
     * Generates REST documentation.
     *
     * @param classes the classes for which to generate the documentation
     * @return the generated documentation
     */
    public ResourceDocType generate(final ClassDoc[] classes) {
        final ResourceDocType resourceDoc = new ResourceDocType();
        populate(resourceDoc, classes);
        return resourceDoc;
    }

    private void populate(final ResourceDocType resourceDoc, final ClassDoc[] classes) {
        final List<ClassDocType> classDocTypes = stream(classes)
                .map(this::asClassDocType)
                .collect(toList());
        if (!classDocTypes.isEmpty()) {
            // Guarded by !isEmpty because calling getDocs() lazy-initialises
            // an internal list and thereby affects the generated XML.
            resourceDoc.getDocs().addAll(classDocTypes);
        }
    }

    private ClassDocType asClassDocType(final ClassDoc classDoc) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("Writing class {}", classDoc.qualifiedTypeName());
        }
        final ClassDocType classDocType = new ClassDocType();
        classDocType.setClassName(classDoc.qualifiedTypeName());
        classDocType.setCommentText(classDoc.commentText());
        options.getDocProcessor().processClassDoc(classDoc, classDocType);
        addMethodDocs(classDoc, classDocType);
        return classDocType;
    }

    private void addMethodDocs(final ClassDoc classDoc, final ClassDocType classDocType) {
        final List<MethodDocType> methodDocTypes =
                stream(classDoc.methods())
                        .map(this::asMethodDocType)
                        .collect(toList());
        if (!methodDocTypes.isEmpty()) {
            // Guarded by !isEmpty because calling getMethodDocs() lazy-initialises
            // an internal list and thereby affects the generated XML.
            classDocType.getMethodDocs().addAll(methodDocTypes);
        }
    }

    private MethodDocType asMethodDocType(final MethodDoc methodDoc) {
        final MethodDocType methodDocType = new MethodDocType();
        methodDocType.setMethodName(methodDoc.name());
        methodDocType.setCommentText(methodDoc.commentText());
        options.getDocProcessor().processMethodDoc(methodDoc, methodDocType);

        addParamDocs(methodDoc, methodDocType);

        // Changed by Atlassian
        addRequestRepresentationDoc(methodDoc, methodDocType);

        // Changed by Atlassian
        addResponseDoc(methodDoc, methodDocType);

        return methodDocType;
    }

    private void addResponseDoc(final MethodDoc methodDoc, final MethodDocType methodDocType) {
        final ResponseDocType responseDoc = new ResponseDocType();
        final Tag returnTag = getSingleTagOrNull(methodDoc, "return");
        if (returnTag != null) {
            responseDoc.setReturnDoc(returnTag.text());
        }
        addWadlParamTypes(methodDoc, responseDoc);
        addRepresentations(methodDoc, responseDoc);
        methodDocType.setResponseDoc(responseDoc);
    }

    private void addWadlParamTypes(final MethodDoc methodDoc, final ResponseDocType responseDoc) {
        final List<WadlParamType> wadlParamTypes = stream(methodDoc.tags(TAG_RESPONSE_PARAM))
                .map(tag -> asWadlParamType(tag, methodDoc))
                .collect(toList());
        if (!wadlParamTypes.isEmpty()) {
            // Guarded by !isEmpty because calling getWadlParams() lazy-initialises
            // an internal list and thereby affects the generated XML.
            responseDoc.getWadlParams().addAll(wadlParamTypes);
        }
    }

    private WadlParamType asWadlParamType(final Tag responseParamTag, final ProgramElementDoc methodDoc) {
        final WadlParamType wadlParam = new WadlParamType();
        for (final Tag inlineTag : responseParamTag.inlineTags()) {
            final String tagName = inlineTag.name();
            final String tagText = inlineTag.text();
            // skip empty tags
            if (isEmpty(tagText)) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Skipping empty inline tag of @response.param in method {}: {}",
                            methodDoc.qualifiedName(), tagName);
                }
                continue;
            }
            if ("@name".equals(tagName)) {
                wadlParam.setName(tagText);
            } else if ("@style".equals(tagName)) {
                wadlParam.setStyle(tagText);
            } else if ("@type".equals(tagName)) {
                wadlParam.setType(QName.valueOf(tagText));
            } else if ("@doc".equals(tagName)) {
                wadlParam.setDoc(tagText);
            } else if (LOG.isWarnEnabled()) {
                LOG.warn("Unknown inline tag of @response.param in method {}: {} (value: {})",
                        methodDoc.qualifiedName(), tagName, tagText);
            }
        }
        return wadlParam;
    }

    private void addRepresentations(final MethodDoc methodDoc, final ResponseDocType responseDoc) {
        final Map<String, List<Tag>> tagsByStatus = getResponseRepresentationTags(methodDoc);
        final List<RepresentationDocType> representations = tagsByStatus.entrySet().stream()
                .map(entry -> getRepresentation(entry.getKey(), entry.getValue()))
                .collect(toList());
        if (!representations.isEmpty()) {
            responseDoc.getRepresentations().addAll(representations);
        }
    }

    private RepresentationDocType getRepresentation(final String status, final List<Tag> tags) {
        final RepresentationDocType representationDoc = new RepresentationDocType();
        representationDoc.setStatus(Long.valueOf(status));
        for (final Tag tag : tags) {
            if (tag.name().endsWith(".qname")) {
                representationDoc.setElement(QName.valueOf(tag.text()));
            } else if (tag.name().endsWith(".mediaType")) {
                representationDoc.setMediaType(tag.text());
            } else if (tag.name().endsWith(".example")) {
                representationDoc.setExample(getSerializedExample(tag));
            } else if (tag.name().endsWith(".doc")) {
                representationDoc.setDoc(tag.text());
            } else if (LOG.isWarnEnabled()) {
                LOG.warn("Unknown response representation tag {}", tag.name());
            }
        }
        return representationDoc;
    }

    private void addRequestRepresentationDoc(final MethodDoc methodDoc, final MethodDocType methodDocType) {
        final Tag requestElement = getSingleTagOrNull(methodDoc, "request.representation.qname");
        final Tag requestExample = getSingleTagOrNull(methodDoc, "request.representation.example");
        if (requestElement != null || requestExample != null) {
            final RequestDocType requestDoc = asRequestDocType(requestElement, requestExample, methodDoc);
            methodDocType.setRequestDoc(requestDoc);
        }
    }

    private RequestDocType asRequestDocType(
            @Nullable final Tag requestElement, @Nullable final Tag requestExample, final MethodDoc methodDoc) {
        final RequestDocType requestDoc = new RequestDocType();
        final RepresentationDocType representationDoc = new RepresentationDocType();

        if (requestElement != null) {
            representationDoc.setElement(QName.valueOf(requestElement.text()));
        }

        if (requestExample != null) {
            final String example = getSerializedExample(requestExample);
            if (isNotEmpty(example)) {
                representationDoc.setExample(example);
            } else if (LOG.isWarnEnabled()) {
                LOG.warn("Could not get serialized example for method {}", methodDoc.qualifiedName());
            }
        }

        requestDoc.setRepresentationDoc(representationDoc);
        return requestDoc;
    }

    private static Map<String, List<Tag>> getResponseRepresentationTags(final MethodDoc methodDoc) {
        final Map<String, List<Tag>> tagsByStatus = new HashMap<>();
        for (final Tag tag : methodDoc.tags()) {
            final Matcher matcher = PATTERN_RESPONSE_REPRESENTATION.matcher(tag.name());
            if (matcher.matches()) {
                final String status = matcher.group(1);
                final List<Tag> tags = tagsByStatus.computeIfAbsent(status, key -> new ArrayList<>());
                tags.add(tag);
            }
        }
        return tagsByStatus;
    }

    /**
     * Atlassian addition
     * Searches an <code>@link</code> tag within the inline tags of the specified tags
     * and serializes the referenced instance.
     *
     * @param tag the tag
     * @return see description
     * @author Martin Grotzke
     */
    @Nullable
    private String getSerializedExample(final Tag tag) {
        if (tag == null) {
            return null;
        }
        final Tag[] inlineTags = tag.inlineTags();
        if (inlineTags == null || inlineTags.length == 0) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Have example: {}", print(tag));
            }
            return tag.text();
        }

        for (final Tag inlineTag : inlineTags) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Have inline tag: {}", print(inlineTag));
            }
            if ("@link".equals(inlineTag.name())) {
                if (LOG.isDebugEnabled()) {
                    LOG.debug("Have link: {}", print(inlineTag));
                }
                final SeeTag linkTag = (SeeTag) inlineTag;
                return getSerializedLinkFromTag(linkTag);
            } else if (isNotEmpty(inlineTag.text())) {
                return inlineTag.text();
            }
        }
        return null;
    }

    @Nullable
    private static Tag getSingleTagOrNull(final MethodDoc methodDoc, final String tagName) {
        final Tag[] tags = methodDoc.tags(tagName);
        if (tags != null && tags.length == 1) {
            return tags[0];
        }
        return null;
    }

    private void addParamDocs(final MethodDoc methodDoc, final MethodDocType methodDocType) {
        final Parameter[] parameters = methodDoc.parameters();
        final ParamTag[] paramTags = methodDoc.paramTags();

        if (parameters != null && paramTags != null) {
            final Map<String, Parameter> params = asMap(parameters, Parameter::name);
            final Map<String, ParamTag> tags = asMap(paramTags, ParamTag::parameterName);

            for (Map.Entry<String, Parameter> parameterEntry : params.entrySet()) {
                final Parameter parameter = parameterEntry.getValue();
                final ParamTag paramTag = tags.get(parameterEntry.getKey());
                if (paramTag != null) {
                    methodDocType.getParamDocs().add(getParamDocType(paramTag, parameter));
                }
            }
        }
    }

    private ParamDocType getParamDocType(final ParamTag paramTag, final Parameter parameter) {
        final ParamDocType paramDocType = new ParamDocType();
        paramDocType.setParamName(paramTag.parameterName());
        paramDocType.setCommentText(paramTag.parameterComment());
        options.getDocProcessor().processParamTag(paramTag, parameter, paramDocType);

        final AnnotationDesc[] annotations = parameter.annotations();
        if (annotations != null) {
            final List<AnnotationDocType> annotationDocTypes = stream(annotations)
                    .map(this::getAnnotationDocType)
                    .collect(toList());
            if (!annotationDocTypes.isEmpty()) {
                paramDocType.getAnnotationDocs().addAll(annotationDocTypes);
            }
        }
        return paramDocType;
    }

    private AnnotationDocType getAnnotationDocType(final AnnotationDesc annotationDesc) {
        final AnnotationDocType annotationDocType = new AnnotationDocType();
        final String typeName = annotationDesc.annotationType().qualifiedName();
        annotationDocType.setAnnotationTypeName(typeName);
        for (final AnnotationDesc.ElementValuePair elementValuePair : annotationDesc.elementValues()) {
            final NamedValueType namedValueType = new NamedValueType();
            namedValueType.setName(elementValuePair.element().name());
            namedValueType.setValue(elementValuePair.value().value().toString());
            annotationDocType.getAttributeDocs().add(namedValueType);
        }
        return annotationDocType;
    }

    private static <K, V> Map<K, V> asMap(final V[] values, final Function<V, K> keyMapper) {
        return stream(values).collect(toMap(keyMapper, identity()));
    }

    private String getSerializedLinkFromTag(final SeeTag linkTag) {
        final MemberDoc referencedMember = linkTag.referencedMember();

        if (referencedMember == null) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("Referenced member of @link {} cannot be resolved.", print(linkTag));
            }
            return null; // Addition by Atlassian
        }

        if (!referencedMember.isStatic()) {
            if (LOG.isWarnEnabled()) {
                LOG.warn("Referenced member of @link {} is not static." +
                        " Only references to static members are supported.", print(linkTag));
            }
            return null;
        }

        /* Get referenced example bean
         */
        final ClassDoc containingClass = referencedMember.containingClass();
        final Object object;
        try {
            final Class<?> aClass = Class.forName(
                    containingClass.qualifiedName(), false, currentThread().getContextClassLoader());
            final Field declaredField = aClass.getDeclaredField(referencedMember.name());
            if (referencedMember.isFinal()) {
                declaredField.setAccessible(true);
            }
            object = declaredField.get(null);
            LOG.debug("Got object {}", object);
        } catch (Exception e) {
            LOG.info("Have classloader: {}", ResourceDocletJSON.class.getClassLoader().getClass());
            LOG.info("Have thread classloader {}", currentThread().getContextClassLoader().getClass());
            LOG.info("Have system classloader {}", ClassLoader.getSystemClassLoader().getClass());
            LOG.error("Could not get field {}", referencedMember.qualifiedName(), e);
            return null;
        }

        try {
            return marshallBean(object);
        } catch (Exception e) {
            LOG.error("Couldn't serialize bean to XML: {}", object, e);
            return null;
        }
    }

    // Addition by Atlassian
    private String marshallBean(final Object object) {
        // expand stuff...
        final Collection<String> expand = options.getExpand();
        if (!expand.isEmpty()) {
            new EntityCrawler().crawl(object, new DefaultExpandParameter(expand), getExpanders());
        }

        final JaxbJsonMarshaller m = DefaultJaxbJsonMarshaller.builder()
                .modules(options.getCustomJacksonSerializerModules())
                .build();
        final String result = m.marshal(object);
        LOG.debug("Got marshalled output:\n{}", result);
        return result;
    }

    private static String print(final Tag tag) {
        return tag.getClass() + "[" +
                "firstSentenceTags=" + toCSV(tag.firstSentenceTags()) +
                ", inlineTags=" + toCSV(tag.inlineTags()) +
                ", kind=" + tag.kind() +
                ", name=" + tag.name() +
                ", text=" + tag.text() +
                "]";
    }

    @Nullable
    private static String toCSV(@Nullable final Tag[] items) {
        if (items == null) {
            return null;
        }
        return toCSV(asList(items));
    }

    @Nullable
    private static String toCSV(@Nullable final Collection<Tag> items) {
        if (items == null) {
            return null;
        }
        return items.stream()
                .map(Tag::name)
                .collect(joining(", "));
    }

    // Addition by Atlassian
    private static EntityExpanderResolver getExpanders() {
        return new ChainingEntityExpanderResolver(asList(
                new CollectionEntityExpanderResolver(),
                new ListWrapperEntityExpanderResolver(),
                new ExpandConstraintEntityExpanderResolver(),
                new SelfExpandingExpander.Resolver(),
                new IdentityEntityExpanderResolver()
        ));
    }
}
