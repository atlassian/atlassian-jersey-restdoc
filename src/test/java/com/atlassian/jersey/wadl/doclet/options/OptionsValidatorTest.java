package com.atlassian.jersey.wadl.doclet.options;

import com.sun.javadoc.DocErrorReporter;
import org.junit.Test;

import static com.atlassian.jersey.wadl.doclet.ResourceDocletJSON.validOptions;
import static com.atlassian.jersey.wadl.doclet.options.Option.*;
import static java.util.Arrays.stream;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class OptionsValidatorTest {

    @Test
    public void validOptions_whenOutputOptionIsEmpty_shouldReportError() {
        final String[][] options = {{ CLASSPATH.getKey(), "theClasspath" }};
        assertInvalidOptions(options, "-output <path-to-file> must be specified.");
    }

    @Test
    public void validOptions_whenClasspathOptionIsEmpty_shouldReportError() {
        final String[][] options = {{OUTPUT.getKey(), "theOutput"}};
        assertInvalidOptions(options, "-classpath <path> must be specified.");
    }

    @Test
    public void validOptions_whenOptionalOptionIsEmpty_shouldReportError() {
        // Arrange
        final String[][] options = {
                { OUTPUT.getKey(), "theOutput" },
                { CLASSPATH.getKey(), "theClasspath" },
                { EXPAND.getKey(), "" }
        };
        final DocErrorReporter reporter = mock(DocErrorReporter.class);

        // Act
        final boolean valid = validOptions(options, reporter);

        // Assert
        assertFalse(valid);
        verify(reporter).printError( EXPAND.getKey() + " if provided cannot be blank.");
        verifyNoMoreInteractions(reporter);
    }

    @Test
    public void validOptions_whenOptionalOptionIsNotEmpty_shouldNotReportError() {
        // Arrange
        final String[][] options = {
                { OUTPUT.getKey(), "theOutput" },
                { CLASSPATH.getKey(), "theClasspath" },
                { EXPAND.getKey(), "theExpand" }
        };
        final DocErrorReporter reporter = mock(DocErrorReporter.class);

        // Act
        final boolean valid = validOptions(options, reporter);

        // Assert
        assertTrue(valid);
        verifyNoMoreInteractions(reporter);
    }

    @Test
    public void validOptions_whenOnlyNonOptionalOptionsAreGiven_shouldNotReportError() {
        // Arrange
        final String[][] options = {
                { OUTPUT.getKey(), "theOutput" },
                { CLASSPATH.getKey(), "theClasspath" }
        };
        final DocErrorReporter reporter = mock(DocErrorReporter.class);

        // Act
        final boolean valid = validOptions(options, reporter);

        // Assert
        assertTrue(valid);
        verifyNoMoreInteractions(reporter);
    }

    private static void assertInvalidOptions(final String[][] options, final String... expectedErrors) {
        // Arrange
        final DocErrorReporter reporter = mock(DocErrorReporter.class);

        // Act
        final boolean valid = validOptions(options, reporter);

        // Assert
        assertFalse(valid);
        stream(expectedErrors).forEach(error -> verify(reporter).printError(error));
        verifyNoMoreInteractions(reporter);
    }
}
