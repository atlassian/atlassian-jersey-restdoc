package com.atlassian.jersey.wadl.doclet.internal;

import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;

import static java.util.Collections.emptySet;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;

public class XmlSerializerTest {

    private static final String CHARSET = StandardCharsets.UTF_8.name();

    @Test
    public void serialize_whenGivenEmptyResourceDocType_shouldSuccessfullySerializeIt() throws Exception {
        // Arrange
        final XmlSerializer xmlSerializer = new XmlSerializer(true);
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        // Act
        final boolean success = xmlSerializer.serialize(new ResourceDocType(), outputStream, emptySet());

        // Assert
        assertTrue(success);
        final String xml = outputStream.toString(CHARSET);
        assertThat(xml, is("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<resourceDoc/>\n"));
    }
}
