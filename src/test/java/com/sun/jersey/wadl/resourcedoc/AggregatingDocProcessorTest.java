package com.sun.jersey.wadl.resourcedoc;

import com.sun.javadoc.ClassDoc;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ClassDocType;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class AggregatingDocProcessorTest {

    @Test
    public void add_whenCalledRepeatedly_shouldRegisterSeparateDocProcessors() {
        // Arrange
        final DocProcessor docProcessor1 = mock(DocProcessor.class);
        final DocProcessor docProcessor2 = mock(DocProcessor.class);
        final AggregatingDocProcessor aggregatingDocProcessor =
                new AggregatingDocProcessor(asList(docProcessor1, docProcessor2));
        final ClassDoc classDoc = mock(ClassDoc.class);
        final ClassDocType classDocType = mock(ClassDocType.class);

        // Act
        aggregatingDocProcessor.processClassDoc(classDoc, classDocType);

        // Assert
        verify(docProcessor1).processClassDoc(classDoc, classDocType);
        verify(docProcessor2).processClassDoc(classDoc, classDocType);
        assertThat(aggregatingDocProcessor.getDocProcessors(), hasItems(docProcessor1, docProcessor2));
    }
}