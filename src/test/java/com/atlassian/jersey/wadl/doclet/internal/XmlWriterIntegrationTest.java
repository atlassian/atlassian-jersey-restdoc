package com.atlassian.jersey.wadl.doclet.internal;

import com.sun.jersey.server.wadl.generators.resourcedoc.model.ClassDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.MethodDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.junit.MockitoJUnit;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptySet;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * An integration test of {@link XmlWriter}, using a real {@link XmlSerializer}.
 */
public class XmlWriterIntegrationTest {

    private static final String FILE_ENCODING = UTF_8.name();

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    private XmlWriter xmlWriter;

    @Before
    public void setUp() {
        xmlWriter = new XmlWriter(new XmlSerializer(true));
    }

    @XmlRootElement
    private static class MyJaxbObject {}

    @Test
    public void write_whenGivenArbitraryJaxbObject_shouldGenerateExpectedXml() throws Exception {
        assertGeneratedDoc(new MyJaxbObject(), "arbitrary-object.xml");
    }

    @Test
    public void write_whenNoClassesAreGiven_shouldGenerateExpectedXml() throws Exception {
        assertGeneratedDoc(new ResourceDocType(), "no-classes.xml");
    }

    @Test
    public void write_whenOneClassIsGiven_shouldGenerateExpectedXml() throws Exception {
        final ResourceDocType resourceDocType = new ResourceDocType();
        resourceDocType.getDocs().add(new ClassDocType());
        assertGeneratedDoc(resourceDocType, "one-class-no-methods.xml");
    }

    @Test
    public void write_whenOneClassAndOneMethodIsGiven_shouldGenerateExpectedXml() throws Exception {
        final MethodDocType methodDocType = new MethodDocType();
        methodDocType.setMethodName("theMethodName");
        final ClassDocType classDocType = new ClassDocType();
        classDocType.getMethodDocs().add(methodDocType);
        final ResourceDocType resourceDocType = new ResourceDocType();
        resourceDocType.getDocs().add(classDocType);
        assertGeneratedDoc(resourceDocType, "one-class-one-method.xml");
    }

    private void assertGeneratedDoc(final Object object, final String expectedFileName) throws Exception {
        // Arrange
        final File outputFile = temporaryFolder.newFile();

        // Act
        final boolean success = xmlWriter.write(object, outputFile.getAbsolutePath(), emptySet());

        // Assert
        assertTrue(success);
        final String expectedXml = loadFileFromTestPackage(expectedFileName);
        final String generatedXml = readFileToString(outputFile, FILE_ENCODING);
        assertThat(generatedXml, is(expectedXml));
    }

    private static String loadFileFromTestPackage(final String filename) throws IOException {
        final InputStream inputStream = ResourceDocGeneratorTest.class.getResourceAsStream(filename);
        assertNotNull("Can't load " + filename, inputStream);
        return IOUtils.toString(inputStream, FILE_ENCODING);
    }
}
