package com.atlassian.jersey.wadl.doclet;

import com.atlassian.jersey.wadl.doclet.options.Option;
import com.atlassian.jersey.wadl.doclet.util.RestoreContextClassLoader;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.RootDoc;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.rules.TestRule;

import java.io.File;

import static java.lang.Thread.currentThread;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ResourceDocletJSONTest {

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Rule
    public final TestRule restoreOriginalContextClassLoader = new RestoreContextClassLoader();

    private String[][] getMinimalOptions() throws Exception {
        final File outputFile = temporaryFolder.newFile();
        return new String[][] {
                {Option.OUTPUT.getKey(), outputFile.getAbsolutePath()}
        };
    }

    @Test
    public void start_whenComplete_shouldRestoreContextClassLoader() throws Exception {
        // Arrange
        final ClassLoader originalClassLoader = mock(ClassLoader.class);
        final RootDoc rootDoc = mock(RootDoc.class);

        currentThread().setContextClassLoader(originalClassLoader);
        final String[][] options = getMinimalOptions();
        when(rootDoc.options()).thenReturn(options);
        when(rootDoc.classes()).thenReturn(new ClassDoc[0]);

        // Act
        @SuppressWarnings("unused")
        final boolean ignored = ResourceDocletJSON.start(rootDoc);

        // Assert
        assertThat(currentThread().getContextClassLoader(), is(originalClassLoader));
    }
}
