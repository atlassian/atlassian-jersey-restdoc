package com.atlassian.jersey.wadl.doclet.options;

import com.sun.jersey.wadl.resourcedoc.AggregatingDocProcessor;
import com.sun.jersey.wadl.resourcedoc.DocProcessor;
import org.codehaus.jackson.map.Module;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.atlassian.jersey.wadl.doclet.options.Option.CLASSPATH;
import static com.atlassian.jersey.wadl.doclet.options.Option.DOC_PROCESSORS;
import static com.atlassian.jersey.wadl.doclet.options.Option.EXPAND;
import static com.atlassian.jersey.wadl.doclet.options.Option.MODULES;
import static com.atlassian.jersey.wadl.doclet.options.Option.OUTPUT;
import static java.lang.Thread.currentThread;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.apache.commons.lang3.StringUtils.split;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

/**
 * Parses the user-provided options.
 *
 * @since 2.0
 */
public class OptionsParser {

    private static final Logger LOGGER = LoggerFactory.getLogger(OptionsParser.class);

    /**
     * Parses the user options from the given array of options and arguments. This input is assumed to be valid, since
     * the Javadoc framework should not invoke this method's caller if its {@code validOptions} method returned false.
     *
     * This method replaces the current thread's context {@link ClassLoader}. It is the caller's responsibility to keep
     * a reference to the original classloader and reinstate it later.
     *
     * @param input an array where each element is an array of the form { option, val1, val2, etc. }
     * @return the parsed value object
     * @see Option#getKey() for the valid option names
     */
    public Options parse(final String[][] input) {
        final URL[] classpath = getClasspath(input);
        // Switch the classloader so that classes named in the user-specified classpath can be loaded
        currentThread().setContextClassLoader(new URLClassLoader(classpath, getClass().getClassLoader()));
        return new Options(
                getOutput(input),
                classpath,
                getCustomJacksonSerializerModules(input),
                getExpand(input),
                getDocProcessor(input)
        );
    }

    private static String getOutput(final String[][] options) {
        return OUTPUT.getValue(options)
                .orElseThrow(() -> new IllegalStateException("No output value was provided"));
    }

    private static Collection<String> getExpand(final String[][] options) {
        final String expandString = EXPAND.getValue(options).orElse("");
        return asList(split(expandString, '&'));
    }

    private static URL[] getClasspath(final String[][] options) {
        final String classpathOption = CLASSPATH.getValue(options).orElse("");
        LOGGER.debug("Classpath option: {}", classpathOption);
        final String[] classpathElements = split(classpathOption, ':');
        return getURLs(classpathElements);
    }

    private static URL[] getURLs(final String[] paths) {
        return stream(paths)
                .map(File::new)
                .map(OptionsParser::toURL)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toArray(URL[]::new);
    }

    private static Optional<URL> toURL(final File file) {
        try {
            return Optional.of(file.toURI().toURL());
        } catch (final MalformedURLException e) {
            return Optional.empty();
        }
    }

    private static DocProcessor getDocProcessor(final String[][] options) {
        // Changed by Atlassian: adding support for multiple doc processors
        final String docProcessorClasses = DOC_PROCESSORS.getValue(options).orElse(null);
        final List<DocProcessor> docProcessors = instantiateAll(DocProcessor.class, docProcessorClasses);
        return new AggregatingDocProcessor(docProcessors);
    }

    private static List<Module> getCustomJacksonSerializerModules(final String[][] options) {
        return instantiateAll(Module.class, MODULES.getValue(options).orElse(null));
    }

    private static <T> List<T> instantiateAll(final Class<T> superType, @Nullable final String implClassNames) {
        final String[] moduleNames = split(trimToEmpty(implClassNames), ':');
        return stream(moduleNames)
                .map(moduleName -> instantiateOne(superType, moduleName))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
    }

    private static <T> Optional<T> instantiateOne(final Class<T> superType, final String implClassName) {
        try {
            final Class<?> implClass =
                    Class.forName(implClassName, true, currentThread().getContextClassLoader());
            final Class<? extends T> classToInstantiate = implClass.asSubclass(superType);
            return Optional.of(classToInstantiate.newInstance());
        } catch (Exception e) {
            LOGGER.error("Could not load or instantiate class {}", implClassName, e);
            return Optional.empty();
        }
    }
}
