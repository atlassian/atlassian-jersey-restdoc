package com.atlassian.jersey.wadl.doclet.options;

import com.sun.javadoc.DocErrorReporter;

import java.util.Collection;
import java.util.Optional;

import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;

/**
 * Validates the options provided on the command line.
 *
 * @since 2.0
 */
public class OptionsValidator {

    private final DocErrorReporter reporter;

    public OptionsValidator(final DocErrorReporter reporter) {
        this.reporter = requireNonNull(reporter);
    }

    /**
     * Validates the given options. This is a fairly shallow sanity check, for example
     * it doesn't check that any classes named can actually be loaded. It simply checks
     * that non-blank values have been provided where necessary.
     *
     * @param options the options to validate
     * @return true if the options are valid
     */
    public boolean validate(final String[][] options) {
        final Collection<String> errors = stream(Option.values())
                .map(option -> option.validate(options))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(toList());
        errors.forEach(reporter::printError);
        return errors.isEmpty();
    }
}
