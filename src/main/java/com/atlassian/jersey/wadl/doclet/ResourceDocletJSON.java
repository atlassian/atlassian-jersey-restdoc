package com.atlassian.jersey.wadl.doclet;

import com.atlassian.jersey.wadl.doclet.internal.ResourceDocGenerator;
import com.atlassian.jersey.wadl.doclet.internal.XmlSerializer;
import com.atlassian.jersey.wadl.doclet.internal.XmlWriter;
import com.atlassian.jersey.wadl.doclet.options.Option;
import com.atlassian.jersey.wadl.doclet.options.Options;
import com.atlassian.jersey.wadl.doclet.options.OptionsParser;
import com.atlassian.jersey.wadl.doclet.options.OptionsValidator;
import com.sun.javadoc.DocErrorReporter;
import com.sun.javadoc.RootDoc;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;

import static java.lang.Thread.currentThread;

/**
 * A custom Javadoc <a href="https://docs.oracle.com/javase/8/docs/technotes/guides/javadoc/doclet/overview.html">doclet</a>
 * that creates a resourcedoc XML file. The ResourceDoc file contains the Javadoc documentation of REST
 * resource classes, so that this can be used for extending generated WADL with useful documentation.
 *
 * Because of the static methods that the Javadoc framework expects each doclet to implement, this class is
 * simply a facade for a few small, highly cohesive classes that are easier to test independently.
 *
 * @since 2.0
 */
@SuppressWarnings("unused")
public final class ResourceDocletJSON {

    /**
     * Return array length for given option: 1 + the number of arguments that the option takes.
     *
     * This method is invoked by the Javadoc framework.
     *
     * @param option the option
     * @return the number of args for the specified option
     */
    public static int optionLength(final String option) {
        return Option.length(option);
    }

    /**
     * Validates the given options.
     *
     * Invoked automatically by the Javadoc framework.
     *
     * @param options the options
     * @param reporter the reporter
     * @return {@code true} the specified options are valid
     */
    public static boolean validOptions(final String[][] options, final DocErrorReporter reporter) {
        return new OptionsValidator(reporter).validate(options);
    }

    /**
     * Starts this doclet. This method is required for any doclet.
     *
     * @param root the root
     * @return true if no exception is thrown
     */
    public static boolean start(final RootDoc root) {
        final ClassLoader originalContextClassLoader = currentThread().getContextClassLoader();
        final Options options = new OptionsParser().parse(root.options());
        try {
            final ResourceDocType resourceDocs = new ResourceDocGenerator(options).generate(root.classes());
            final XmlSerializer xmlSerializer = new XmlSerializer(false);
            return new XmlWriter(xmlSerializer).write(resourceDocs, options.getOutput(), options.getExtraJaxbContextClasses());
        } finally {
            currentThread().setContextClassLoader(originalContextClassLoader);
        }
    }

    private ResourceDocletJSON() {
        throw new UnsupportedOperationException("Not for instantiation");
    }
}
