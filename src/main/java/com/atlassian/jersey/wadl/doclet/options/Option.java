package com.atlassian.jersey.wadl.doclet.options;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Arrays.stream;
import static java.util.Objects.requireNonNull;
import static org.apache.commons.lang3.StringUtils.isBlank;

/**
 * An option that the user must/can provide when using this doclet.
 *
 * @since 2.0
 */
public enum Option {

    /**
     * The classpath from which to load any user-specified classes.
     * <br>
     * A colon-delimited list of file/directory paths, for use by a {@link java.net.URLClassLoader}.
     */
    CLASSPATH("classpath", "<path>", 2, true),

    DOC_PROCESSORS("processors", "<processor1[:processor2[...]]>", 2, false),

    EXPAND("expand", "<expand1[&expand2[...]]>", 2, false),  // Changed by Atlassian

    /**
     * Any custom Jackson serializer modules the user wishes to invoke.
     * <br>
     * A colon-delimited list of classes that extend {@link org.codehaus.jackson.map.Module}.
     * <br>
     * Added by Atlassian.
     */
    MODULES("modules", "<module1[:module2[...]]>", 2, false),

    /**
     * The name and path of the file to generate.
     */
    OUTPUT("output", "<path-to-file>", 2, true);

    private static final Logger LOGGER = LoggerFactory.getLogger(Option.class);

    /**
     * Returns the array length for the given option: 1 + the number of arguments that the option takes.
     *
     * This method is invoked by the Javadoc framework.
     *
     * @param option the option
     * @return the number of args for the specified option
     */
    public static int length(final String option) {
        LOGGER.debug("Invoked with option {}", option);
        return stream(values())
                .filter(value -> value.key.equalsIgnoreCase(option))
                .findFirst()
                .map(o -> o.length)
                .orElse(0);
    }

    private final String key;
    private final String argumentFormat;
    private final int length;
    private final boolean mandatory;

    Option(final String key, final String argumentFormat, final int length, final boolean mandatory) {
        this.key = "-" + requireNonNull(key);
        this.length = length;
        this.mandatory = mandatory;
        this.argumentFormat = requireNonNull(argumentFormat);
    }

    /**
     * Exists only to make tests easier to write and maintain.
     *
     * @return the key for this option
     */
    public String getKey() {
        return key;
    }

    /**
     * Returns the value of this option from the given array, looking for the key in the first element of each inner
     * array (case-insensitive) and getting the value from the second element of the first matching array.
     *
     * @param options the array of option names and values from which to extract the value of this option
     * @return empty if there's no inner array with the key as the first element and the same length as this option
     */
    public Optional<String> getValue(final String[][] options) {
        return stream(options)
                .filter(array -> array.length == this.length)
                .filter(array -> key.equalsIgnoreCase(array[0]))
                .map(array -> array[1])
                .filter(Objects::nonNull)
                .findFirst();
    }

    /**
     * Validates the given value for this option.
     *
     * @param options each element is an array of the form { optionKey, optionValue1, optionValue2, etc. }
     * @return an error message if validation fails
     */
    public Optional<String> validate(final String[][] options) {
        final String value = getValue(options).orElse(null);
        if (mandatory && isBlank(value)) {
            return Optional.of(format("%s %s must be specified.", key, argumentFormat));
        }
        if (!mandatory && value != null && isBlank(value)) {
            return Optional.of(key + " if provided cannot be blank.");
        }
        return Optional.empty();
    }
}
