package com.atlassian.jersey.wadl.doclet.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Collection;

import static java.util.Objects.requireNonNull;

/**
 * Writes objects to an XML file.
 *
 * @since 2.0
 */
public class XmlWriter {

    private static final Logger LOGGER = LoggerFactory.getLogger(XmlWriter.class);

    // The use of the '^' between the namespaceURI and the localname seems to be an implementation detail of the
    // Xerces code. When processing XML that doesn't use namespaces, simply omit the namespace prefix, as shown in
    // the third CDataElement.
//    private static final String[] DEFAULT_CDATA_ELEMENTS = { "ns1^commentText", "ns2^commentText", "^commentText" };

    private final XmlSerializer xmlSerializer;

    public XmlWriter(final XmlSerializer xmlSerializer) {
        this.xmlSerializer = requireNonNull(xmlSerializer);
    }

    /**
     * Writes the given object to the output file specified by the options passed to the constructor.
     *
     * @param object the object to write to XML
     * @param filename the file to which to write the XML
     * @param extraJaxbContextClasses any classes needed to serialize the object, apart from its own class
     * @return {@code true} if successful
     */
    public boolean write(final Object object, final String filename, final Collection<Class<?>> extraJaxbContextClasses) {
        try {
            try (final OutputStream out = new BufferedOutputStream(new FileOutputStream(filename))) {
                //        DEFAULT_CDATA_ELEMENTS, options.getDocProcessor().getCDataElements()
                //        outputFormat.setPreserveSpace(true);
                //        outputFormat.setIndenting(true);
                final boolean success = xmlSerializer.serialize(object, out, extraJaxbContextClasses);
                if (success) {
                    LOGGER.info("Wrote {}", filename);
                }
                return success;
            }
        } catch (final Exception e) {
            LOGGER.error("Could not serialize object", e);
            return false;
        }
    }
}
