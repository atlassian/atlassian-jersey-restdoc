package com.atlassian.jersey.wadl.doclet.util;

import org.junit.rules.ExternalResource;

import static java.lang.Thread.currentThread;

/**
 * A {@link org.junit.rules.TestRule} that restores the original context {@link ClassLoader} after the test is complete.
 */
public class RestoreContextClassLoader extends ExternalResource {

    private ClassLoader originalContextClassLoader;

    @Override
    protected void before() {
        originalContextClassLoader = currentThread().getContextClassLoader();
    }

    @Override
    protected void after() {
        if (originalContextClassLoader != null) {
            currentThread().setContextClassLoader(originalContextClassLoader);
        }
    }
}
