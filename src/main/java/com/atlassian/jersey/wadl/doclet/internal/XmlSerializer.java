package com.atlassian.jersey.wadl.doclet.internal;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.dom.DOMResult;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.lang.String.format;
import static javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT;

/**
 * Serialises JAXB-compliant objects into XML.
 *
 * @since 2.0
 */
@ParametersAreNonnullByDefault
public class XmlSerializer {

    private final boolean prettyPrint;

    public XmlSerializer(final boolean prettyPrint) {
        this.prettyPrint = prettyPrint;
    }

    /**
     * Serializes the given object to XML, sending the result to the given destination.
     *
     * @param object the object to serialize
     * @param destination the destination
     * @param extraJaxbContextClasses any extra classes needed for JAXB marshalling
     * @return {@code true} if serialization was successful
     * @throws JAXBException if the object could not be marshalled into a {@link Node}
     */
    public boolean serialize(
            final Object object, final OutputStream destination, final Collection<Class<?>> extraJaxbContextClasses)
            throws JAXBException {
        final DOMImplementationLS lsFactory = getLSObjectFactory();
        final LSSerializer serializer = getLsSerializer(lsFactory);
        final LSOutput output = getLsOutput(lsFactory, destination);
        final JAXBContext jaxbContext = getJaxbContext(object.getClass(), extraJaxbContextClasses);
        final Node node = marshall(object, jaxbContext);
        return serializer.write(node, output);
    }

    private static LSOutput getLsOutput(final DOMImplementationLS lsFactory, final OutputStream outputStream) {
        final LSOutput output = lsFactory.createLSOutput();
        output.setByteStream(outputStream);
        return output;
    }

    private LSSerializer getLsSerializer(final DOMImplementationLS lsFactory) {
        final LSSerializer serializer = lsFactory.createLSSerializer();
        serializer.getDomConfig().setParameter("format-pretty-print", prettyPrint);
        return serializer;
    }

    @Nonnull
    private static DOMImplementationLS getLSObjectFactory() {
        try {
            final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
            final String features = "LS";
            final DOMImplementation domImplementation = registry.getDOMImplementation(features);
            if (domImplementation == null) {
                throw new IllegalStateException(format("No DOM implementation with features '%s'", features));
            }
            return (DOMImplementationLS) domImplementation;
        } catch (final ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            throw new IllegalStateException("Could not load DOM implementation", e);
        }
    }

    private static Node marshall(final Object object, final JAXBContext jaxbContext) throws JAXBException {
        final DOMResult domResult = new DOMResult();
        final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(object, domResult);
        return domResult.getNode();
    }

    private static JAXBContext getJaxbContext(
            final Class<?> objectClass, final Collection<Class<?>> extraJaxbContextClasses)
            throws JAXBException {
        final List<Class<?>> jaxbContextClasses = new ArrayList<>();
        jaxbContextClasses.add(objectClass);
        jaxbContextClasses.addAll(extraJaxbContextClasses);
        return JAXBContext.newInstance(jaxbContextClasses.toArray(new Class<?>[0]));
    }
}
