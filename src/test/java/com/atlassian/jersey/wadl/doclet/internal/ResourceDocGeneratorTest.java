package com.atlassian.jersey.wadl.doclet.internal;

import com.atlassian.jersey.wadl.doclet.options.Options;
import com.sun.javadoc.ClassDoc;
import com.sun.javadoc.MethodDoc;
import com.sun.javadoc.Tag;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ClassDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.MethodDocType;
import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import com.sun.jersey.wadl.resourcedoc.DocProcessor;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import java.util.List;

import static com.atlassian.jersey.wadl.doclet.internal.ResourceDocGenerator.TAG_RESPONSE_PARAM;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("SameParameterValue")
public class ResourceDocGeneratorTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private DocProcessor docProcessor;

    @Mock
    private Options options;

    @InjectMocks
    private ResourceDocGenerator resourceDocGenerator;

    @Test
    public void generate_whenRootContainsNoClasses_shouldGenerateCorrectDocs() {
        // Arrange
        final ClassDoc[] classes = {};

        // Act
        final ResourceDocType docs = resourceDocGenerator.generate(classes);

        // Assert
        assertThat(docs.getDocs(), hasSize(0));
    }

    @Test
    public void generate_whenRootContainsOneClass_shouldGenerateCorrectDocs() {
        // Arrange
        final String className = "MyResource";
        final String classComment = "theClassComment";
        final String methodName = "theMethodName";
        final String methodComment = "theMethodComment";
        final MethodDoc methodDoc = mockMethodDoc(methodName, methodComment);
        final ClassDoc classDoc = mockClassDoc(className, classComment, methodDoc);
        final ClassDoc[] classDocs = {classDoc};
        when(options.getDocProcessor()).thenReturn(docProcessor);

        // Act
        final ResourceDocType docs = resourceDocGenerator.generate(classDocs);

        // Assert
        assertThat(docs.getDocs(), hasSize(1));
        final ClassDocType classDocType = docs.getDocs().get(0);
        assertThat(classDocType.getClassName(), is(className));
        assertThat(classDocType.getCommentText(), is(classComment));
        final List<MethodDocType> methodDocs = classDocType.getMethodDocs();
        assertThat(methodDocs, hasSize(1));
        final MethodDocType methodDocType = methodDocs.get(0);
        assertThat(methodDocType.getMethodName(), is(methodName));
        assertThat(methodDocType.getCommentText(), is(methodComment));
        verify(docProcessor).processClassDoc(classDoc, classDocType);
    }

    private static ClassDoc mockClassDoc(
            final String qualifiedTypeName, final String commentText, final MethodDoc... methods) {
        final ClassDoc classDoc = mock(ClassDoc.class);
        when(classDoc.qualifiedTypeName()).thenReturn(qualifiedTypeName);
        when(classDoc.commentText()).thenReturn(commentText);
        when(classDoc.methods()).thenReturn(methods);
        return classDoc;
    }

    private static MethodDoc mockMethodDoc(final String name, final String commentText) {
        final MethodDoc methodDoc = mock(MethodDoc.class);
        when(methodDoc.name()).thenReturn(name);
        when(methodDoc.commentText()).thenReturn(commentText);
        when(methodDoc.tags()).thenReturn(new Tag[0]);
        when(methodDoc.tags(TAG_RESPONSE_PARAM)).thenReturn(new Tag[0]);
        return methodDoc;
    }
}
