package com.atlassian.jersey.wadl.doclet.options;

import com.sun.jersey.wadl.resourcedoc.DocProcessor;
import org.codehaus.jackson.map.Module;

import javax.annotation.Nonnull;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;

import static java.util.Collections.unmodifiableCollection;
import static java.util.Objects.requireNonNull;

/**
 * The user-provided options for running this doclet.
 *
 * Instances of this value object are immutable.
 *
 * @since 2.0
 */
public class Options {

    private final Collection<Module> customJacksonSerializerModules;
    private final Collection<String> expand;
    private final DocProcessor docProcessor;
    private final String output;
    private final URL[] classpath;

    public Options(final String output, final URL[] classpath, final Collection<Module> customJacksonSerializerModules,
                   final Collection<String> expand, final DocProcessor docProcessor) {
        this.classpath = classpath.clone();
        this.customJacksonSerializerModules = unmodifiableCollection(customJacksonSerializerModules);
        this.docProcessor = requireNonNull(docProcessor);
        this.expand = unmodifiableCollection(expand);
        this.output = requireNonNull(output);
    }

    @Nonnull
    public URL[] getClasspath() {
        return classpath;
    }

    @Nonnull
    public Collection<Module> getCustomJacksonSerializerModules() {
        return customJacksonSerializerModules;
    }

    @Nonnull
    public DocProcessor getDocProcessor() {
        return docProcessor;
    }

    @Nonnull
    public Collection<String> getExpand() {
        return expand;
    }

    @Nonnull
    public Collection<Class<?>> getExtraJaxbContextClasses() {
        return Arrays.asList(docProcessor.getRequiredJaxbContextClasses());
    }

    @Nonnull
    public String getOutput() {
        return output;
    }
}
