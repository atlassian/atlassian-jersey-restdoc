package com.atlassian.jersey.wadl.doclet.internal;

import com.sun.jersey.server.wadl.generators.resourcedoc.model.ResourceDocType;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.MethodRule;
import org.junit.rules.TemporaryFolder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Collection;

import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class XmlWriterTest {

    @Rule
    public final MethodRule mockitoRule = MockitoJUnit.rule();

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Mock
    private ResourceDocType resourceDoc;

    @Mock
    private XmlSerializer xmlSerializer;

    @InjectMocks
    private XmlWriter xmlWriter;

    @Test
    public void write_whenSerializationSucceeds_shouldReturnTrue() throws Exception {
        // Arrange
        final Collection<Class<?>> extraJaxbContextClasses = singleton(MyContextClass.class);
        final File outputFile = temporaryFolder.newFile();
        when(xmlSerializer.serialize(eq(resourceDoc), any(), eq(extraJaxbContextClasses))).thenReturn(true);

        // Act
        final boolean success = xmlWriter.write(resourceDoc, outputFile.getAbsolutePath(), extraJaxbContextClasses);

        // Assert
        assertThat(success, is(true));
        verify(xmlSerializer).serialize(same(resourceDoc), any(), eq(extraJaxbContextClasses));
    }

    @Test
    public void write_whenSerializationFails_shouldReturnFalse() throws Exception {
        // Arrange
        final Collection<Class<?>> extraJaxbContextClasses = emptySet();
        final File outputFile = temporaryFolder.newFile();
        when(xmlSerializer.serialize(same(resourceDoc), any(), eq(extraJaxbContextClasses))).thenReturn(false);

        // Act
        final boolean success = xmlWriter.write(resourceDoc, outputFile.getAbsolutePath(), extraJaxbContextClasses);

        // Assert
        assertThat(success, is(false));
    }

    @Test
    public void write_whenSerializationThrowsException_shouldReturnFalse() throws Exception {
        // Arrange
        final Collection<Class<?>> extraJaxbContextClasses = emptySet();
        final File outputFile = temporaryFolder.newFile();
        when(xmlSerializer.serialize(same(resourceDoc), any(), eq(extraJaxbContextClasses)))
                .thenThrow(new JAXBException("Dummy exception"));

        // Act
        final boolean success = xmlWriter.write(resourceDoc, outputFile.getAbsolutePath(), extraJaxbContextClasses);

        // Assert
        assertThat(success, is(false));
    }

    private static class MyContextClass {}
}
