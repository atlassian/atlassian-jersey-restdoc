package com.atlassian.jersey.wadl.doclet.options;

import org.junit.Test;

import static com.atlassian.jersey.wadl.doclet.ResourceDocletJSON.optionLength;
import static org.junit.Assert.assertEquals;

public class ResourceDocletJSONOptionLengthTest {

    @Test
    public void optionLength_whenOptionIsClasspath_shouldReturnTwo() {
        assertEquals(2, optionLength("-classpath"));
    }

    @Test
    public void optionLength_whenOptionIsExpand_shouldReturnTwo() {
        assertEquals(2, optionLength("-expand"));
    }

    @Test
    public void optionLength_whenOptionIsOutput_shouldReturnTwo() {
        assertEquals(2, optionLength("-output"));
    }

    @Test
    public void optionLength_whenOptionIsModules_shouldReturnTwo() {
        assertEquals(2, optionLength("-modules"));
    }

    @Test
    public void optionLength_whenOptionIsDocProcessors_shouldReturnTwo() {
        assertEquals(2, optionLength("-processors"));
    }

    @Test
    public void optionLength_whenOptionIsAnythingElse_shouldReturnZero() {
        assertEquals(0, optionLength("noSuchOption"));
    }
}
